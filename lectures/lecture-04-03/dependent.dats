#include "share/atspre_define.hats"
#include "share/atspre_staload.hats"

#include "share/atspre_staload_libats_ML.hats"

//we always need this to make c happy
implement main0() = ()

// we reviewed some intuition for the Curry-Howard corespondence. Now lets put it into practice in ATS.
// slides at https://docs.google.com/presentation/d/1Es3923vAnmPLUi-XR-CdAe6R-uXWNuvlIyoyh6QdYQs/edit?usp=sharing

// first we do everything with the old constructs
datatype GTE    (int, int)=
  | {x:int}   eq(x  ,x   ) of ()
  | {x,y:int} gt(x  ,y   ) of (GTE(x-1,y))


val proof1gte1 = eq() : GTE(1,1)
val proof2gte2 = eq() : GTE(2,2)

val proof2gte1 = gt(proof1gte1) : GTE(2,1)

val proof3gte1 = gt(
                    gt(
                       eq() //: GTE(1,1)
                    )       //: GTE(2,1)
                 )            : GTE(3,1)


fun isGreaterThenZero{n:nat}(nAgain:int(n)): GTE(n,0) =
  if nAgain = 0
  then eq()
  else gt(isGreaterThenZero(nAgain-1))

// but recursion lets us prove wrong things
fun proof1gte2(): GTE(1,2) = proof1gte2()
val proof1gte2 = proof1gte2() : GTE(1,2)


dataprop GTE    (int, int)=
  | {x:int}   eq(x  ,x   ) of ()
  | {x,y:int} gt(x  ,y   ) of (GTE(x-1,y))


prval proof1gte1 = eq() : GTE(1,1)
prval proof2gte2 = eq() : GTE(2,2)

prval proof2gte1 = gt(proof1gte1) : GTE(2,1)

prval proof3gte1 = gt(
gt(
eq() //: GTE(1,1)
) //: GTE(2,1)
) : GTE(3,1)

//def is_greater_then_zero(n):
//    if n == 0:
//        print("0 = 0")
//    else:
//        print(n, ">= 0 because", n - 1, ">=0")
//        is_greater_then_zero(n - 1)

prfun isGreaterThenZero{n:nat} .< n >. (nAgain:int(n)): GTE(n,0) =
  if nAgain = 0
  then eq()
  else gt(isGreaterThenZero(nAgain-1))

//prfun proof1gte2(): GTE(1,2) = proof1gte2()
//prval proof1gte2 = proof1gte2() : GTE(1,2)



// definition of lists
datatype myList (A:t@ype) =
  | nil  of ()
  | cons of (A,myList(A))

//from this definition we can build concrete lists
val ls1 = cons(1,cons(2,cons(3,nil()))) : myList(int)
val ls2 = cons('h',cons('i',nil() ))    : myList(char)
//...

// we can define functions over lists
fun {A:t@ype} get(ls: myList(A), i:int): A =
  if i = 0
  then case- ls of
        | cons(head, _) => head
  else  case- ls of
        | cons(_, tail) => get(tail, i-1)

// or
fun {A:t@ype} get(ls: myList(A), i:int): A =
  case- ls of
    | cons(head, tail) =>
      if i = 0
      then head
      else  get(tail, i-1)


datatype myList (A:t@ype, int) =
  |          nil(A      , 0  ) of ()
  | {n:nat} cons(A      , n+1) of (A, myList(A,n))



val ls1 = cons(1,cons(2,cons(3,nil()))) : myList(int, 3)
val ls2 = cons('h',cons('i',nil() ))    : myList(char, 2)


fun {A:t@ype}get{n, i:nat | n > i }(ls:myList(A,n), i:int(i)): A =
  case+ ls of
    | cons(a, tail) => if i = 0 then a else get(tail, i-1)

val () = println!(get(ls1,1))

//val () = println!(get(ls1,4))   //gives a compile error

//val () = println!(get(nil(),0)) //gives a compile error

