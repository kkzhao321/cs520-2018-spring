(* ****** ****** *)
//
// Title:
// Principles of
// Programming Languages
// Course: CAS CS 520
//
// Semester: Spring, 2018
//
// Classroom: MCS B25
// Class Time: TR 2:00-3:15
//
// Instructor: Hongwei Xi (hwxiATcsDOTbuDOTedu)
//
(* ****** ****** *)
//
// Due date: Tuesday, the 1st of May
//
(* ****** ****** *)
//
// HX: 30 points in total
//
(* ****** ****** *)

viewdef at(a:t@ype, l:addr) = a@l

(* ****** ****** *)

dataview
myarray1(a:t@ype, addr, int) =
| {l:addr}
  myarray1_nil(a, l, 0) of ()
| {l:addr}{n:nat}
  myarray1_cons(a, l, n+1) of
  (a@l, myarray1(a, l+sizeof(a), n))

(* ****** ****** *)

dataview
myarray2(a:t@ype, addr, int) =
| {l:addr}
  myarray2_nil(a, l, 0) of ()
| {l:addr}{n:nat}
  myarray2_cons(a, l, n+1) of
  (myarray2(a, l, n), at(a, l+n*sizeof(a)))

(* ****** ****** *)
//
// HX: 20 points for the following two:
//
extern
prfun
{a:t@ype}
from_myarray1_to_myarray2
  {l:addr}{n:nat}(pf: myarray1(a, l, n)): myarray2(a, l, n)
//
extern
prfun
{a:t@ype}
from_myarray2_to_myarray1
  {l:addr}{n:nat}(pf: myarray2(a, l, n)): myarray1(a, l, n)
//
(* ****** ****** *)
//
// HX: Please implement the following function // 10 points
//
extern
fun
{a:t@ype}
myarray1_initialize
{l:addr}{n:nat}
( pf: !myarray1(a?, l, n) >> myarray1(a, l, n)
| p0: ptr(l), n: int(n), fwork: (&a? >> a) -<cloref1> void): void
//
(* ****** ****** *)

(* end of [assign08.dats] *)
