(* ****** ****** *)

datatype stypx =
  | STYPXbas of string
  | STYPXvar of ref(stypxopt)
  | STYPXtup of (stypx, stypx)
  | STYPXfun of (stypx, stypx)

where stypxopt = Option(stypx)

(* ****** ****** *)

typedef stypx_var = ref(stypxopt)

(* ****** ****** *)

val int_t = STYPXbas("int")
val bool_t = STYPXbas("bool")
val string_t = STYPXbas("string")

(* ****** ****** *)

#define :: list0_cons

val
theSig =
g0ofg1
(
$list{$tup(string, list0(stypx), stypx)}
(
$tup("+", int_t :: int_t :: nil0(), int_t)
,
$tup("-", int_t :: int_t :: nil0(), int_t)
,
$tup("*", int_t :: int_t :: nil0(), int_t)
,
$tup("/", int_t :: int_t :: nil0(), int_t)
,
$tup("mod", int_t :: int_t :: nil0(), int_t)
)
)
(* ****** ****** *)

extern
fun
print_stypx
(T: stypx): void
overload print with print_stypx
extern
fun
fprint_stypx
(out: FILEref, T: stypx): void
overload fprint with fprint_stypx

(* ****** ****** *)

implement
print_stypx(T) =
fprint_stypx(stdout_ref, T)

implement
fprint_stypx(out, T) =
(
case+ T of
| STYPXbas(nm) =>
  fprint!(out, "STYbas(", nm, ")")
| STYPXtup(T1, T2) =>
  fprint!(out, "STYPXtup(", T1, "*", T2, ")")
| STYPXfun(T1, T2) =>
  fprint!(out, "STYPXfun(", T1, "->", T2, ")")
| STYPXvar(X) =>
  (
    case+ !X of
    | None() => fprint!(out, "STYPXvar(None)")
    | Some(T) => fprint!(out, "STYPXvar(Some(", T, "))")
  )
)

(* ****** ****** *)

extern
fun
stypx_solve(T1: stypx, T2: stypx): bool

extern
fun
stypx_solve_var(X: stypx_var, T2: stypx): bool

(* ****** ****** *)

datatype expr =
//
  | EXPint of (int)
  | EXPstr of string
  | EXPbool of (bool)
//
  | EXPsnd of (expr)
  | EXPfst of (expr)
  | EXPtup of (expr, expr)
//
  | EXPvar of string
  | EXPapp of (expr(*fun*), expr(*arg*))
  | EXPlam of (string(*x*), stypxopt, expr(*body*))
  | EXPfix of (string(*f*), string(*x*), stypxopt(*arg*), stypxopt(*ret*), expr(*body*))
//
  | EXPifb of (expr(*test*), expr(*then*), expr(*else*))
// end of [expr]

(* ****** ****** *)

typedef
tyctx = list0($tup(string(*x*), stypx(*T*)))

extern
fun
stypxinfer(Gamma: tyctx, e0: expr): stypxopt
extern
fun
stypxinfer_var(Gamma: tyctx, e0: expr): stypxopt
extern
fun
stypxinfer_tup(Gamma: tyctx, e0: expr): stypxopt
extern
fun
stypxinfer_fst(Gamma: tyctx, e0: expr): stypxopt
extern
fun
stypxinfer_snd(Gamma: tyctx, e0: expr): stypxopt
extern
fun
stypxinfer_app(Gamma: tyctx, e0: expr): stypxopt
extern
fun
stypxinfer_lam(Gamma: tyctx, e0: expr): stypxopt
extern
fun
stypxinfer_fix(Gamma: tyctx, e0: expr): stypxopt
extern
fun
stypxinfer_ifb(Gamma: tyctx, e0: expr): stypxopt

(* ****** ****** *)

implement
stypxinfer(Gamma, e0) =
(
case+ e0 of
//
| EXPint _ => Some(int_t)
| EXPstr _ => Some(string_t)
| EXPbool _ => Some(bool_t)
//
| EXPvar _ => stypxinfer_var(Gamma, e0)
//
| EXPtup _ => stypxinfer_tup(Gamma, e0)
| EXPfst _ => stypxinfer_fst(Gamma, e0)
| EXPsnd _ => stypxinfer_snd(Gamma, e0)
//
| EXPlam _ => stypxinfer_lam(Gamma, e0)
| EXPapp _ => stypxinfer_app(Gamma, e0)
//
| EXPfix _ => stypxinfer_fix(Gamma, e0)
//
| EXPifb _ => stypxinfer_ifb(Gamma, e0)
//
)

(* ****** ****** *)

(* end of [STFPX.dats] *)
