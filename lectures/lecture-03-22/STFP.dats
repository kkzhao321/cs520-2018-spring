(* ****** ****** *)

#include
"share/atspre_staload.hats"
#include
"share/atspre_staload_libats_ML.hats"

(* ****** ****** *)

datatype styp =
  | STYbas of string
  | STYtup of (styp, styp)
  | STYfun of (styp, styp)

(* ****** ****** *)

val int_t = STYbas("int")
val string_t = STYbas("string")

(* ****** ****** *)

#define :: list0_cons

val
theSig =
g0ofg1
(
$list{$tup(string, list0(styp), styp)}
(
$tup("+", int_t :: int_t :: nil0(), int_t)
,
$tup("-", int_t :: int_t :: nil0(), int_t)
,
$tup("*", int_t :: int_t :: nil0(), int_t)
,
$tup("/", int_t :: int_t :: nil0(), int_t)
,
$tup("mod", int_t :: int_t :: nil0(), int_t)
)
)
(* ****** ****** *)

extern
fun
print_styp
(T: styp): void
overload print with print_styp
extern
fun
fprint_styp
(out: FILEref, T: styp): void
overload fprint with fprint_styp

(* ****** ****** *)

implement
print_styp(T) =
fprint_styp(stdout_ref, T)

implement
fprint_styp(out, T) =
(
case+ T of
| STYbas(nm) =>
  fprint!(out, "STYbas(", nm, ")")
| STYtup(T1, T2) =>
  fprint!(out, "STYtup(", T1, "*", T2, ")")
| STYfun(T1, T2) =>
  fprint!(out, "STYfun(", T1, "->", T2, ")")
)

(* ****** ****** *)
//
extern
fun
eq_styp_styp
  : (styp, styp) -> bool
//
overload = with eq_styp_styp
//
(* ****** ****** *)

implement
eq_styp_styp
  (st1, st2) =
(
case+ (st1, st2) of
| ( STYbas nm1
  , STYbas nm2) => nm1 = nm2
| ( STYtup(st11, st12)
  , STYtup(st21, st22)) =>
     (st11 = st21) && (st12 = st22)
| ( STYfun(st11, st12)
  , STYfun(st21, st22)) =>
     (st11 = st21) && (st12 = st22)
| (_, _) => false
)

(* ****** ****** *)

datatype expr =
//
  | EXPint of (int)
  | EXPstr of string
//
  | EXPsnd of (expr)
  | EXPfst of (expr)
  | EXPtup of (expr, expr)
//
  | EXPvar of string
  | EXPapp of (expr(*fun*), expr(*arg*))
  | EXPlam of (string(*x*), styp, expr(*body*))
  | EXPfix of (string(*f*), string(*x*), styp(*arg*), styp(*ret*), expr(*body*))
// end of [expr]

(* ****** ****** *)

typedef
tyctx = list0($tup(string(*x*), styp(*T*)))

extern
fun
stypcheck(Gamma: tyctx, e0: expr): Option(styp)
extern
fun
stypcheck_var(Gamma: tyctx, e0: expr): Option(styp)
extern
fun
stypcheck_tup(Gamma: tyctx, e0: expr): Option(styp)
extern
fun
stypcheck_fst(Gamma: tyctx, e0: expr): Option(styp)
extern
fun
stypcheck_snd(Gamma: tyctx, e0: expr): Option(styp)
extern
fun
stypcheck_app(Gamma: tyctx, e0: expr): Option(styp)
extern
fun
stypcheck_lam(Gamma: tyctx, e0: expr): Option(styp)
extern
fun
stypcheck_fix(Gamma: tyctx, e0: expr): Option(styp)

(* ****** ****** *)

implement
stypcheck(Gamma, e0) =
(
case+ e0 of
| EXPint _ => Some(STYbas("int"))
| EXPstr _ => Some(STYbas("string"))
| EXPvar _ => stypcheck_var(Gamma, e0)
| EXPtup _ => stypcheck_tup(Gamma, e0)
| EXPfst _ => stypcheck_fst(Gamma, e0)
| EXPsnd _ => stypcheck_snd(Gamma, e0)
| EXPlam _ => stypcheck_lam(Gamma, e0)
| EXPapp _ => stypcheck_app(Gamma, e0)
| EXPfix _ => stypcheck_fix(Gamma, e0)
)

(* ****** ****** *)

implement
stypcheck_var
  (Gamma, e0) = let
  val-EXPvar(x) = e0
  val opt =
  list0_find_opt(Gamma, lam(xt) => x = xt.0)
in
  case+ opt of
  | ~None_vt() => None()
  | ~Some_vt(xt) => Some(xt.1)
end // end of [stypcheck_var]

(* ****** ****** *)

implement
stypcheck_tup
  (Gamma, e0) = let
  val-EXPtup(e1, e2) = e0
  val opt1 = stypcheck(Gamma, e1)
  val opt2 = stypcheck(Gamma, e2)
in
  case+ opt1 of
  | None() =>
    None()
  | Some(T1) =>
    (
      case+ opt2 of
      | None() => None()
      | Some(T2) => Some(STYtup(T1, T2))
    )
end

(* ****** ****** *)

implement
stypcheck_fst
  (Gamma, e0) = let
  val-EXPfst(e1) = e0
  val opt1 = stypcheck(Gamma, e1)
in
  case+ opt1 of
  | None() =>
    None()
  | Some(T1) =>
    (
      case+ T1 of
      | STYtup(T11, _) => Some(T11) | _ => None()
    )
end

(* ****** ****** *)

implement
stypcheck_snd
  (Gamma, e0) = let
  val-EXPsnd(e2) = e0
  val opt2 = stypcheck(Gamma, e2)
in
  case+ opt2 of
  | None() =>
    None()
  | Some(T2) =>
    (
      case+ T2 of
      | STYtup(_, T22) => Some(T22) | _ => None()
    )
end

(* ****** ****** *)

implement
stypcheck_lam
  (Gamma, e0) = let
//
  val-EXPlam(x1, T1, e2) = e0
//
  val
  Gamma =
  list0_cons($tup(x1, T1), Gamma)
  val opt2 = stypcheck(Gamma, e2)
//
in
  case+ opt2 of
  | None() => None()
  | Some(T2) => Some(STYfun(T1, T2))
end // end of [stypcheck_lam]

(* ****** ****** *)

implement
stypcheck_fix
  (Gamma, e0) = let
//
  val-
  EXPfix(f, x, T1, T2, e2) = e0
//
  val Tf = STYfun(T1, T2)
  val
  Gamma =
  list0_cons($tup(f, Tf), Gamma)
  val
  Gamma =
  list0_cons($tup(x, T1), Gamma)
  val opt2 = stypcheck(Gamma, e2)
//
in
  case+ opt2 of
  | None() => None()
  | Some(T3) => if T2 = T3 then Some(T2) else None()
end // end of [stypcheck_lam]

(* ****** ****** *)

implement
stypcheck_app
  (Gamma, e0) = let
  val-EXPapp(e1, e2) = e0
  val opt1 = stypcheck(Gamma, e1)
  val opt2 = stypcheck(Gamma, e2)
in
  case+ opt1 of
  | None() =>
    None()
  | Some(T1) =>
    (
      case+ T1 of
      | STYtup(T11, T12) =>
        (
          case+ opt2 of
          | None() =>
            None()
          | Some(T2) =>
            if T11 = T2 then Some(T12) else None()
        )
      | _ => None((*void*))
    )
end

(* ****** ****** *)

val prog = EXPlam("x", int_t, EXPvar("x"))
val-Some(T0) = stypcheck(nil0(), prog)
val () = println! ("T0 = ", T0)

(* ****** ****** *)

val omega = // lam x. x(x)
EXPlam("x", int_t, EXPapp(x, x)) where { val x = EXPvar("x") }
val-None() = stypcheck(nil0(), omega)

(* ****** ****** *)

val swap =
EXPlam
( "xy"
, STYtup(int_t, int_t)
, EXPtup(EXPsnd(xy), EXPfst(xy))
) where { val xy = EXPvar("xy") }

val-Some(T_swap) = stypcheck(nil0(), swap)

val () = println! ("T_swap = ", T_swap)

(* ****** ****** *)

implement main0() = ()

(* ****** ****** *)

(* end of [STFP.dats] *)
