(* ****** ****** *)

#include
"share/atspre_staload.hats"
#include
"share/atspre_staload_libats_ML.hats"

(* ****** ****** *)

#staload UN = "prelude/SATS/unsafe.sats"

(* ****** ****** *)

datatype stypx =
  | STYPXbas of string
  | STYPXtup of (stypx, stypx)
  | STYPXfun of (stypx, stypx)
  | STYPXvar of stypx_var

where stypx_var = ref(Option(stypx))

(* ****** ****** *)

val int_t = STYPXbas("int")
val string_t = STYPXbas("string")

(* ****** ****** *)

extern
fun
stypx_is_var(T: stypx): bool

implement
stypx_is_var(T) =
(
case+ T of
| STYPXvar(X) =>
  (
  case+ !X of
  | None() => true
  | Some(T2) => stypx_is_var(T2)
  )
| _(*non-STYPXvar*) => false
)

(* ****** ****** *)
//
extern
fun
stypx_occurs_var
  (X: stypx_var, T: stypx): bool
//
(* ****** ****** *)

implement
stypx_occurs_var
  (X, T) =
(
case+ T of
| STYPXbas _ => false
| STYPXtup(T1, T2) =>
  stypx_occurs_var(X, T1) orelse stypx_occurs_var(X, T2)
| STYPXfun(T1, T2) =>
  stypx_occurs_var(X, T1) orelse stypx_occurs_var(X, T2)
| STYPXvar(X2) =>
  (
  case+ !X2 of
  | None() => stypx_var_eq(X, X2)
  | Some(T2) => stypx_occurs_var(X, T2)
  )
) where
{

fun
stypx_var_eq
(X1: stypx_var, X2: stypx_var): bool = ref_get_ptr(X1) = ref_get_ptr(X2)

}

(* ****** ****** *)

extern
fun
stypx_solve(T1: stypx, T2: stypx): bool

extern
fun
stypx_solve_var(X: stypx_var, T2: stypx): bool

(* ****** ****** *)

implement
stypx_solve(T1, T2) =
(
case+ (T1, T2) of
| (STYPXvar X1, _) =>
  stypx_solve_var(X1, T2)
| (T1, STYPXvar X2) =>
  stypx_solve_var(X2, T1)
| (STYPXbas(nm1), STYPXbas(nm2)) => nm1 = nm2
| (STYPXtup(T11, T12), STYPXtup(T21, T22)) =>
  stypx_solve(T11, T21) andalso stypx_solve(T12, T22)
| (STYPXfun(T11, T12), STYPXfun(T21, T22)) =>
  stypx_solve(T11, T21) andalso stypx_solve(T12, T22)
| (_, _) => false
)

(* ****** ****** *)

implement
stypx_solve_var
  (X1, T2) =
(
case+ !X1 of
| None() =>
  if
  stypx_occurs_var(X1, T2)
  then
  (if stypx_is_var(T2) then true else false)
  else (!X1 := Some(T2); true)
| Some(T1) => stypx_solve(T1, T2)
)

(* ****** ****** *)

val X1 = ref<Option(stypx)>(None)
val X2 = ref<Option(stypx)>(None)

(* ****** ****** *)

(*
val a1 = stypx_solve_var(X1, int_t)
val () = println! ("a1 = ", a1)
val a2 = stypx_solve_var(X1, string_t)
val () = println! ("a2 = ", a2)
*)

(* ****** ****** *)

(*
val F_X1_X2 =
STYPXfun(STYPXvar(X1), STYPXvar(X2))
val a3 = stypx_solve_var(X1, F_X1_X2)
val () = println! ("a3 = ", a3)
*)

(* ****** ****** *)

val a4 =
stypx_solve(STYPXvar(X1), STYPXvar(X2))
val () = println! ("a4 = ", a4)
val a5 =
stypx_solve(STYPXvar(X2), STYPXfun(STYPXvar(X1), int_t))
val () = println! ("a5 = ", a5)

(* ****** ****** *)

implement main0() = ()

(* ****** ****** *)

(* end of [lambda.dats] *)
