def is_greater_then_zero(n):
    if n == 0:
        print("0 = 0")
    else:
        print(n, ">= 0 because", n - 1, ">=0")
        is_greater_then_zero(n - 1)


is_greater_then_zero(100)
