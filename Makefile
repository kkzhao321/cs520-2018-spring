######
#
# A simple Makefile
#
######

all::

######
#
# Adding a upstream:
#
# git remote add upstream \
#   https://bithwxi@bitbucket.org/bithwxi/cs520-2018-spring.git
#
######
#
# update:: ; \
# 	git fetch upstream
#	git checkout master
#	git merge upstream/master
#
######
#
git-push:: ; \
  git push -u https://bithwxi@bitbucket.org/bithwxi/cs520-2018-spring.git
#
######
