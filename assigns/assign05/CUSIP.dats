(* ****** ****** *)
//
// Title:
// Principles of
// Programming Languages
// Course: CAS CS 520
//
// Semester: Spring, 2018
//
// Classroom: MCS B25
// Class Time: TR 2:00-3:15
//
// Instructor: Hongwei Xi (hwxiATcsDOTbuDOTedu)
//
(* ****** ****** *)
//
// Due date: Tuesday, the 13th of March
//
(* ****** ****** *)

(*
Please read:
https://en.wikipedia.org/wiki/CUSIP

Here are some examples:

037833100 Apple Incorporated
17275R102 Cisco Systems
38259P508 Google Incorporated
594918104 Microsoft Corporation
68389X106 Oracle Corporation (incorrect)
68389X105 Oracle Corporation 

*)

(* ****** ****** *)

(*
//
// HX: 10 points
//
Please implement the following function for
checking the validity of a given CUSIP number
//
*)

extern
fun
cusip_check(cusip: string): bool

(* ****** ****** *)

(* end of [CUSIP.dats] *)
