(* ****** ****** *)
//
// Title:
// Principles of
// Programming Languages
// Course: CAS CS 520
//
// Semester: Spring, 2018
//
// Classroom: MCS B25
// Class Time: TR 2:00-3:15
//
// Instructor: Hongwei Xi (hwxiATcsDOTbuDOTedu)
//
(* ****** ****** *)
//
// Due date: Tuesday, the 13th of March
//
(* ****** ****** *)

(*
//
// HX: 10 points
//
Please do this one for ATS:
http://rosettacode.org/wiki/Use_another_language_to_call_a_function
//
*)

(* ****** ****** *)

#define ATS_MAINATSFLAG 1

(* ****** ****** *)

#include
"./../Call-ATS-from-C.dats"

(* ****** ****** *)

// (*
//
// HX: A dummy implementation
//
implement Query(Data, Length) = 0
//
// *)
//
(* ****** ****** *)

(* end of [Call-ATS-from-C.dats] *)
