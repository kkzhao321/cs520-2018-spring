(* ****** ****** *)

#include "./../assign03.dats"

(* ****** ****** *)

(*
Please do your implementation below:
*)

(* ****** ****** *)
//
// HX:
// The following testing code is provided:
//
implement
main0() =
{
//
val () =
println!
("IsPrime(73) = ", TMapp(IsPrime, TMint(73)))
//
val () =
println!
("NumberOfPrimes(100) = ", TMapp(NumberOfPrimes, TMint(100)))
//
} (* end of [main0] *)

(* ****** ****** *)

(* end of [assign03_sol.dats] *)