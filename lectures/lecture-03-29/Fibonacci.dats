
#include
"share/atspre_staload.hats"
#include
"share/atspre_staload_libats_ML.hats"


(*
Fibs:
0, 1, 1, 2, 3, 5, 8, 13, 21, ...
*)

dataprop FIB(int, int) = 
  | FIB0(0, 0) of () // FIB0 : () -> FIB(0, 0)
  | FIB1(1, 1) of () // FIB1 : () -> FIB(1, 1)
  | {n:int | n >= 2}{r1,r2:int}
    FIB2(n, r1+r2) of (FIB(n-1, r1), FIB(n-2, r2))

(* ****** ****** *)

(*
extern
fun
print_FIB
{n:int}{r:int}
(pf: FIB(n, r)): void
extern
fun
fprint_FIB
{n:int}{r:int}
(out: FILEref, pf: FIB(n, r)): void

overload print with print_FIB
overload fprint with fprint_FIB
*)

(* ****** ****** *)

(*
implement
print_FIB(pf) =
fprint_FIB(stdout_ref, pf)

implement
fprint_FIB(out, pf0) =
(
case+ pf0 of
| FIB0() => fprint(out, "FIB0")
| FIB1() => fprint(out, "FIB1")
| FIB2(pf1, pf2) => fprint!(out, "FIB2(", pf1, ", ", pf2, ")")
)
*)

(* ****** ****** *)

extern
fun
fib
{n:nat}
(x: int(n)): [r:int] (FIB(n, r) | int(r))

implement
fib(x) =
ifcase
| x = 0 => (FIB0() | 0)
| x = 1 => (FIB1() | 1)
| _ (*else*) => let
    val res1 = fib(x-1)
    val res2 = fib(x-2)
  in
    (FIB2(res1.0, res2.0) | res1.1 + res2.1)
  end
  

(* ****** ****** *)

val fib5 = fib(5)

(* ****** ****** *)

(*
val () = println!("Proof: fib(5) = ", fib5.0)
*)
val () = println!("Value: fib(5) = ", fib5.1)

(* ****** ****** *)

extern
fun
fib2
{n:nat}
(x: int(n)): [r:int] (FIB(n, r) | int(r))

(* ****** ****** *)

implement
fib2{n}(x) = let
//
// f1 = fib(y+1), f2 = fib(y)
//
fun      
loop
{i:nat | i <= n}
{r1,r2:int}
( pf1: FIB(i+1, r1)
, pf2: FIB(i+0, r2)
| y: int(i)
, f1: int(r1), f2: int(r2)
) : [r:int] (FIB(n, r) | int(r)) =
  if y < x
   then loop(FIB2(pf1, pf2), pf1 | y+1, f1+f2, f1)
   else (pf2 | f2)
//
in
  loop(FIB1(), FIB0() | 0, 1, 0)
end // end of [fib2]

(* ****** ****** *)

implement main0() = ()

(* ****** ****** *)

(* end of [Fibonacci.dats] *)
