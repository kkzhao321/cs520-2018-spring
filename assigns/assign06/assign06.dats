(* ****** ****** *)

#include
"share/atspre_staload.hats"
#include
"share/atspre_staload_libats_ML.hats"

(* ****** ****** *)

datatype styp =
  | STYPbas of string
  | STYPtup of (styp, styp)
  | STYPfun of (styp, styp)

(* ****** ****** *)

val int_t = STYPbas("int")
val string_t = STYPbas("string")

(* ****** ****** *)

#define :: list0_cons

val
theSig =
g0ofg1
(
$list{$tup(string, list0(styp), styp)}
(
$tup("+", int_t :: int_t :: nil0(), int_t)
,
$tup("-", int_t :: int_t :: nil0(), int_t)
,
$tup("*", int_t :: int_t :: nil0(), int_t)
,
$tup("/", int_t :: int_t :: nil0(), int_t)
,
$tup("mod", int_t :: int_t :: nil0(), int_t)
)
)
(* ****** ****** *)

extern
fun
print_styp
(T: styp): void
overload print with print_styp
extern
fun
fprint_styp
(out: FILEref, T: styp): void
overload fprint with fprint_styp

(* ****** ****** *)

implement
print_styp(T) =
fprint_styp(stdout_ref, T)

implement
fprint_styp(out, T) =
(
case+ T of
| STYPbas(nm) =>
  fprint!(out, "STYPbas(", nm, ")")
| STYPtup(T1, T2) =>
  fprint!(out, "STYPtup(", T1, "*", T2, ")")
| STYPfun(T1, T2) =>
  fprint!(out, "STYPfun(", T1, "->", T2, ")")
)

(* ****** ****** *)
//
extern
fun
eq_styp_styp
  : (styp, styp) -> bool
//
overload = with eq_styp_styp
//
(* ****** ****** *)

implement
eq_styp_styp
  (st1, st2) =
(
case+ (st1, st2) of
| ( STYPbas nm1
  , STYPbas nm2) => nm1 = nm2
| ( STYPtup(st11, st12)
  , STYPtup(st21, st22)) =>
     (st11 = st21) && (st12 = st22)
| ( STYPfun(st11, st12)
  , STYPfun(st21, st22)) =>
     (st11 = st21) && (st12 = st22)
| (_, _) => false
)

(* ****** ****** *)

datatype expr =
//
  | EXPRint of (int)
  | EXPRstr of string
//
  | EXPRsnd of (expr)
  | EXPRfst of (expr)
  | EXPRtup of (expr, expr)
//
  | EXPRvar of string
  | EXPRapp of (expr(*fun*), expr(*arg*))
  | EXPRlam of (string(*x*), styp, expr(*body*))
  | EXPRfix of (string(*f*), string(*x*), styp(*arg*), styp(*ret*), expr(*body*))
//
  | EXPRopr of (string(*opr*), list0(expr))
// end of [expr]

(* ****** ****** *)

typedef
tyctx = list0($tup(string(*x*), styp(*T*)))

extern
fun
stypcheck(Gamma: tyctx, e0: expr): Option(styp)
extern
fun
stypcheck_var(Gamma: tyctx, e0: expr): Option(styp)
extern
fun
stypcheck_tup(Gamma: tyctx, e0: expr): Option(styp)
extern
fun
stypcheck_fst(Gamma: tyctx, e0: expr): Option(styp)
extern
fun
stypcheck_snd(Gamma: tyctx, e0: expr): Option(styp)
extern
fun
stypcheck_app(Gamma: tyctx, e0: expr): Option(styp)
extern
fun
stypcheck_lam(Gamma: tyctx, e0: expr): Option(styp)
extern
fun
stypcheck_fix(Gamma: tyctx, e0: expr): Option(styp)

(* ****** ****** *)
//
extern
fun
stypcheck_opr(Gamma: tyctx, e0: expr): Option(styp)
//
(* ****** ****** *)

implement
stypcheck(Gamma, e0) =
(
case+ e0 of
| EXPRint _ => Some(STYPbas("int"))
| EXPRstr _ => Some(STYPbas("string"))
| EXPRvar _ => stypcheck_var(Gamma, e0)
| EXPRtup _ => stypcheck_tup(Gamma, e0)
| EXPRfst _ => stypcheck_fst(Gamma, e0)
| EXPRsnd _ => stypcheck_snd(Gamma, e0)
| EXPRlam _ => stypcheck_lam(Gamma, e0)
| EXPRapp _ => stypcheck_app(Gamma, e0)
| EXPRfix _ => stypcheck_fix(Gamma, e0)
| EXPRopr _ => stypcheck_opr(Gamma, e0)
)

(* ****** ****** *)

implement
stypcheck_var
  (Gamma, e0) = let
  val-EXPRvar(x) = e0
  val opt =
  list0_find_opt(Gamma, lam(xt) => x = xt.0)
in
  case+ opt of
  | ~None_vt() => None()
  | ~Some_vt(xt) => Some(xt.1)
end // end of [stypcheck_var]

(* ****** ****** *)

implement
stypcheck_tup
  (Gamma, e0) = let
  val-EXPRtup(e1, e2) = e0
  val opt1 = stypcheck(Gamma, e1)
  val opt2 = stypcheck(Gamma, e2)
in
  case+ opt1 of
  | None() =>
    None()
  | Some(T1) =>
    (
      case+ opt2 of
      | None() => None()
      | Some(T2) => Some(STYPtup(T1, T2))
    )
end

(* ****** ****** *)

implement
stypcheck_fst
  (Gamma, e0) = let
  val-EXPRfst(e1) = e0
  val opt1 = stypcheck(Gamma, e1)
in
  case+ opt1 of
  | None() =>
    None()
  | Some(T1) =>
    (
      case+ T1 of
      | STYPtup(T11, _) => Some(T11) | _ => None()
    )
end

(* ****** ****** *)

implement
stypcheck_snd
  (Gamma, e0) = let
  val-EXPRsnd(e2) = e0
  val opt2 = stypcheck(Gamma, e2)
in
  case+ opt2 of
  | None() =>
    None()
  | Some(T2) =>
    (
      case+ T2 of
      | STYPtup(_, T22) => Some(T22) | _ => None()
    )
end

(* ****** ****** *)

implement
stypcheck_lam
  (Gamma, e0) = let
//
  val-EXPRlam(x1, T1, e2) = e0
//
  val
  Gamma =
  list0_cons($tup(x1, T1), Gamma)
  val opt2 = stypcheck(Gamma, e2)
//
in
  case+ opt2 of
  | None() => None()
  | Some(T2) => Some(STYPfun(T1, T2))
end // end of [stypcheck_lam]

(* ****** ****** *)

implement
stypcheck_fix
  (Gamma, e0) = let
//
  val-
  EXPRfix(f, x, T1, T2, e2) = e0
//
  val Tf = STYPfun(T1, T2)
  val
  Gamma =
  list0_cons($tup(f, Tf), Gamma)
  val
  Gamma =
  list0_cons($tup(x, T1), Gamma)
  val opt2 = stypcheck(Gamma, e2)
//
in
  case+ opt2 of
  | None() => None()
  | Some(T3) => if T2 = T3 then Some(T2) else None()
end // end of [stypcheck_lam]

(* ****** ****** *)

implement
stypcheck_app
  (Gamma, e0) = let
  val-EXPRapp(e1, e2) = e0
  val opt1 = stypcheck(Gamma, e1)
  val opt2 = stypcheck(Gamma, e2)
in
  case+ opt1 of
  | None() =>
    None()
  | Some(T1) =>
    (
      case+ T1 of
      | STYPtup(T11, T12) =>
        (
          case+ opt2 of
          | None() =>
            None()
          | Some(T2) =>
            if T11 = T2 then Some(T12) else None()
        )
      | _ => None((*void*))
    )
end

(* ****** ****** *)

(* end of [assign06.dats] *)
