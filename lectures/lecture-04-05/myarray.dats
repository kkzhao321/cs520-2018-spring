(* ****** ****** *)

dataview
myarray(a:t@ype, addr, int) =
| {l:addr}
  myarray_nil(a, l, 0) of ()
| {l:addr}{n:nat}
  myarray_cons(a, l, n+1) of (a@l, myarray(a, l+sizeof(a), n))

(* ****** ****** *)

extern
fun
{a:t@ype}
myarray_get_first
{l:addr}{n:pos}
(pf: !myarray(a, l, n) | p0: ptr(l)): a

(* ****** ****** *)

implement
{a}
myarray_get_first
  (pf | p0) = let
//
prval
myarray_cons(pf1, pf2) = pf
//
val res = ptr_get<a>(pf1 | p0)
//
in
  pf := myarray_cons(pf1, pf2); res
end // end of [myarray_get_first]

(* ****** ****** *)

(* end of [myarray.dats] *)
