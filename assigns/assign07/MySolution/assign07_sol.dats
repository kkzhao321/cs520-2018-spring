(* ****** ****** *)

#include
"share/atspre_staload.hats"
#include
"share/atspre_staload_libats_ML.hats"

(* ****** ****** *)
//
dataprop
FACT(int, int) = 
  | FACTbas(0, 1) of ()
  | {n:nat}{r:int}
    FACTind(n+1, (n+1)*r) of FACT(n, r)
//
(* ****** ****** *)

extern
fun
fact1{n:nat}(int(n)): [r:int] (FACT(n, r) | int(r))
extern
fun
fact2{n:nat}(int(n)): [r:int] (FACT(n, r) | int(r))

(* ****** ****** *)

(*
//
// HX: 10 points
//
Please implement fact1 by following the
following algorithm:
fact1(n) = if n > 0 then n * fact1(n-1) else 1
//
*)

(* ****** ****** *)

(*
//
// HX: 20 points
//
Please implement fact2 by following the
following algorithm:
fact2(n) = loop(n, 1) where
{
  fun loop(n, r) = if n > 0 then loop(n-1, n*r) else r
}
//
*)

(* ****** ****** *)

(* end of [assign07.dats] *)
