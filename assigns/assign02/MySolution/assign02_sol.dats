(* ****** ****** *)

#include "./../assign02.dats"

(* ****** ****** *)

(*
Please do your implementation below:
*)

(* ****** ****** *)
//
// HX:
// The following testing code is provided:
//
implement
main0() =
{
//
val () =
println!
("twicethrice(tmadd)(1) = ", evaluate(TMapp(TMapp(twicethrice,tmadd),TMint(1))))
//
val () =
println!
("thricetwice(tmadd)(1) = ", evaluate(TMapp(TMapp(thricetwice,tmadd),TMint(1))))
//
} (* end of [main0] *)

(* ****** ****** *)

(* end of [assign02_sol.dats] *)