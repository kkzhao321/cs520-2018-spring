(* ****** ****** *)

#include
"share/atspre_staload.hats"
#include
"share/atspre_staload_libats_ML.hats"

(* ****** ****** *)

datatype stypx =
  | STYPXbas of string
  | STYPXtup of (stypx, stypx)
  | STYPXfun of (stypx, stypx)
  | STYPXvar of stypx_var

where stypx_var = ref(Option(stypx))

(* ****** ****** *)

val int_t = STYPXbas("int")
val bool_t = STYPXbas("bool")
val string_t = STYPXbas("string")

(* ****** ****** *)

#define :: list0_cons

val
theSig =
g0ofg1
(
$list{$tup(string, list0(stypx), stypx)}
(
$tup("+", int_t :: int_t :: nil0(), int_t)
,
$tup("-", int_t :: int_t :: nil0(), int_t)
,
$tup("*", int_t :: int_t :: nil0(), int_t)
,
$tup("/", int_t :: int_t :: nil0(), int_t)
,
$tup("mod", int_t :: int_t :: nil0(), int_t)
)
)
(* ****** ****** *)

extern
fun
print_stypx
(T: stypx): void
overload print with print_stypx
extern
fun
fprint_stypx
(out: FILEref, T: stypx): void
overload fprint with fprint_stypx

(* ****** ****** *)

implement
print_stypx(T) =
fprint_stypx(stdout_ref, T)

implement
fprint_stypx(out, T) =
(
case+ T of
| STYPXbas(nm) =>
  fprint!(out, "STYbas(", nm, ")")
| STYPXtup(T1, T2) =>
  fprint!(out, "STYPXtup(", T1, "*", T2, ")")
| STYPXfun(T1, T2) =>
  fprint!(out, "STYPXfun(", T1, "->", T2, ")")
| STYPXvar(X) => fprint!(out, "STYPXvar(...)")
)

(* ****** ****** *)
(*
//
extern
fun
eq_stypx_stypx
  : (stypx, stypx) -> bool
//
overload = with eq_stypx_stypx
//
*)
(* ****** ****** *)

extern
fun
stypx_solve(T1: stypx, T2: stypx): bool

extern
fun
stypx_solve_var(X: stypx_var, T2: stypx): bool

(* ****** ****** *)

datatype expr =
//
  | EXPint of (int)
  | EXPstr of string
//
  | EXPsnd of (expr)
  | EXPfst of (expr)
  | EXPtup of (expr, expr)
//
  | EXPvar of string
  | EXPapp of (expr(*fun*), expr(*arg*))
  | EXPlam of (string(*x*), Option(stypx), expr(*body*))
  | EXPfix of (string(*f*), string(*x*), stypx(*arg*), stypx(*ret*), expr(*body*))
//
  | EXPif of (expr(*test*), expr(*then*), expr(*else*))
// end of [expr]

(* ****** ****** *)

typedef
tyctx = list0($tup(string(*x*), stypx(*T*)))

extern
fun
stypxinfer(Gamma: tyctx, e0: expr): Option(stypx)
extern
fun
stypxinfer_var(Gamma: tyctx, e0: expr): Option(stypx)
extern
fun
stypxinfer_tup(Gamma: tyctx, e0: expr): Option(stypx)
extern
fun
stypxinfer_fst(Gamma: tyctx, e0: expr): Option(stypx)
extern
fun
stypxinfer_snd(Gamma: tyctx, e0: expr): Option(stypx)
extern
fun
stypxinfer_app(Gamma: tyctx, e0: expr): Option(stypx)
extern
fun
stypxinfer_lam(Gamma: tyctx, e0: expr): Option(stypx)
extern
fun
stypxinfer_fix(Gamma: tyctx, e0: expr): Option(stypx)
extern
fun
stypxinfer_if(Gamma: tyctx, e0: expr): Option(stypx)

(* ****** ****** *)

implement
stypxinfer(Gamma, e0) =
(
case+ e0 of
| EXPint _ => Some(STYPXbas("int"))
| EXPstr _ => Some(STYPXbas("string"))
| EXPvar _ => stypxinfer_var(Gamma, e0)
| EXPtup _ => stypxinfer_tup(Gamma, e0)
| EXPfst _ => stypxinfer_fst(Gamma, e0)
| EXPsnd _ => stypxinfer_snd(Gamma, e0)
| EXPlam _ => stypxinfer_lam(Gamma, e0)
| EXPapp _ => stypxinfer_app(Gamma, e0)
| EXPfix _ => stypxinfer_fix(Gamma, e0)
//
| EXPif _ => stypxinfer_if(Gamma, e0)
//
)

(* ****** ****** *)

implement
stypxinfer_var
  (Gamma, e0) = let
  val-EXPvar(x) = e0
  val opt =
  list0_find_opt(Gamma, lam(xt) => x = xt.0)
in
  case+ opt of
  | ~None_vt() => None()
  | ~Some_vt(xt) => Some(xt.1)
end // end of [stypxinfer_var]

(* ****** ****** *)

implement
stypxinfer_tup
  (Gamma, e0) = let
  val-EXPtup(e1, e2) = e0
  val opt1 = stypxinfer(Gamma, e1)
  val opt2 = stypxinfer(Gamma, e2)
in
  case+ opt1 of
  | None() =>
    None()
  | Some(T1) =>
    (
      case+ opt2 of
      | None() => None()
      | Some(T2) => Some(STYPXtup(T1, T2))
    )
end

(* ****** ****** *)

implement
stypxinfer_fst
  (Gamma, e0) = let
  val-EXPfst(e1) = e0
  val opt1 = stypxinfer(Gamma, e1)
in
  case+ opt1 of
  | None() =>
    None()
  | Some(T1) =>
    (
      case+ T1 of
      | STYPXtup(T11, _) => Some(T11) | _ => None()
    )
end

(* ****** ****** *)

implement
stypxinfer_snd
  (Gamma, e0) = let
  val-EXPsnd(e2) = e0
  val opt2 = stypxinfer(Gamma, e2)
in
  case+ opt2 of
  | None() =>
    None()
  | Some(T2) =>
    (
      case+ T2 of
      | STYPXtup(_, T22) => Some(T22) | _ => None()
    )
end

(* ****** ****** *)

implement
stypxinfer_lam
  (Gamma, e0) = let
//
  val-EXPlam(x1, O1, e2) = e0
//
  val T1 =
  (
   case O1 of
   | None() => STYPXvar(ref(None))
   | Some(T1) => T1
  ) : stypx
//
  val
  Gamma =
  list0_cons($tup(x1, T1), Gamma)
  val opt2 = stypxinfer(Gamma, e2)
//
in
  case+ opt2 of
  | None() => None()
  | Some(T2) => Some(STYPXfun(T1, T2))
end // end of [stypxinfer_lam]

(* ****** ****** *)

implement
stypxinfer_fix
  (Gamma, e0) = let
//
  val-
  EXPfix(f, x, T1, T2, e2) = e0
//
  val Tf = STYPXfun(T1, T2)
  val
  Gamma =
  list0_cons($tup(f, Tf), Gamma)
  val
  Gamma =
  list0_cons($tup(x, T1), Gamma)
  val opt2 = stypxinfer(Gamma, e2)
//
in
  case+ opt2 of
  | None() => None()
  | Some(T3) =>
    if stypx_solve(T2, T3) then Some(T2) else None()
end // end of [stypxinfer_lam]

(* ****** ****** *)

implement
stypxinfer_app
  (Gamma, e0) = let
  val-EXPapp(e1, e2) = e0
  val opt1 = stypxinfer(Gamma, e1)
  val opt2 = stypxinfer(Gamma, e2)
in
  case+ opt1 of
  | None() =>
    None()
  | Some(T1) =>
    (
      case+ T1 of
      | STYPXtup(T11, T12) =>
        (
          case+ opt2 of
          | None() =>
            None()
          | Some(T2) =>
            if stypx_solve(T11, T2) then Some(T12) else None()
        )
      | _ => None((*void*))
    )
end

(* ****** ****** *)

implement
stypxinfer_if
  (Gamma, e0) = let
  val-EXPif(e1, e2, e3) = e0
  val opt1 = stypxinfer(Gamma, e1)
  val opt2 = stypxinfer(Gamma, e2)
  val opt3 = stypxinfer(Gamma, e3)
in
  case+ opt1 of
  | None() =>
    None()
  | Some(T1) =>
    (
      if
      stypx_solve(T1, bool_t)
      then
      (
        case+ (opt2, opt3) of
        | (None(), _) => None()
        | (_, None()) => None()
        | (Some(T2), Some(T3)) =>
          if stypx_solve(T2, T3) then Some(T2) else None()
      )
      else None()
    )
end

(* ****** ****** *)

val prog = EXPlam("x", None, EXPvar("x"))
val-Some(T0) = stypxinfer(nil0(), prog)
val () = println! ("T0 = ", T0)

(* ****** ****** *)

val omega = // lam x. x(x)
EXPlam("x", None, EXPapp(x, x)) where { val x = EXPvar("x") }
val-None() = stypxinfer(nil0(), omega)

(* ****** ****** *)

val swap =
EXPlam
( "xy"
, None()
, EXPtup(EXPsnd(xy), EXPfst(xy))
) where { val xy = EXPvar("xy") }

val-Some(T_swap) = stypxinfer(nil0(), swap)

val () = println! ("T_swap = ", T_swap)

(* ****** ****** *)

implement main0() = ()

(* ****** ****** *)

(* end of [STFPX.dats] *)
