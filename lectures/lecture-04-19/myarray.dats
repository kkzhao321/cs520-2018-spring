(* ****** ****** *)

dataview
myarray(a:t@ype, addr, int) =
| {l:addr}
  myarray_nil(a, l, 0) of ()
| {l:addr}{n:nat}
  myarray_cons(a, l, n+1) of (a@l, myarray(a, l+sizeof(a), n))

(*
dataview
myarray2(a:t@ype, addr, int) =
| {l:addr}
  myarray2_nil(a, l, 0) of ()
| {l:addr}{n:nat}
  myarray2_cons(a, l, n+1) of (myarray2(a, l, n), a@(l+n*size(a))
*)

(* ****** ****** *)

extern
prfun
{a:t@ype}
myarray_split
{l:addr}
{n:int}{i:nat|i < n}
( pf: myarray(a, l, n)
, i0: int(i)): (myarray(a, l, i), myarray(a, l+i*sizeof(a), n-i))
extern
prfun
{a:t@ype}
myarray_unsplit
{l:addr}
{n1,n2:nat}
(myarray(a, l, n1), myarray(a, l+n1*sizeof(a), n2)): myarray(a, l, n1+n2)


(* ****** ****** *)

extern
fun
{a:t@ype}
myarray_get_first
{l:addr}{n:pos}
(pf: !myarray(a, l, n) | p0: ptr(l)): a

extern
fun
{a:t@ype}
myarray_get_at
{l:addr}{n:int}{i:nat | i < n}
(pf: !myarray(a, l, n) | p0: ptr(l), i: int(i)): a

(* ****** ****** *)

implement
{a}
myarray_get_first
  (pf | p0) = let
//
prval
myarray_cons(pf1, pf2) = pf
//
val res = ptr_get<a>(pf1 | p0)
//
in
  pf := myarray_cons(pf1, pf2); res
end // end of [myarray_get_first]

(* ****** ****** *)

(*
implement
{a}
myarray_get_at
(pf | p0, i) =
if
(i = 0)
then
(
  myarray_get_first<a>(pf | p0)
)
else let
//
prval
myarray_cons(pf1, pf2) = pf
//
val res = myarray_get_at<a>(pf2 | ptr_succ<a>(p0), i-1)
//
in
  pf := myarray_cons(pf1, pf2); res
end
*)

(* ****** ****** *)

implement
{a}
myarray_get_at
(pf | p0, i) =
if
(i = 0)
then
(
  myarray_get_first<a>(pf | p0)
)
else let
  prval (pf1, pf2) = myarray_split<a>(pf, i)
  val res = myarray_get_first<a>(pf2 | ptr_add<a>(p0, i))
in
  pf := myarray_unsplit<a>(pf1, pf2); res  
end

(* ****** ****** *)

(* end of [myarray.dats] *)
